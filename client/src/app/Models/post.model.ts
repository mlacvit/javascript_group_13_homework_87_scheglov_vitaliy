import { User } from './user.model';

export class PostModel {
  constructor(
    public _id: string,
    public title: string,
    public description: string,
    public image: string,
    public date: string,
    public user: User,
  ) {}
}


export interface PostData {
  title: string;
  description: string,
  image: File,
  user: string
}



