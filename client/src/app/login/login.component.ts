import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../store/types';
import { Observable } from 'rxjs';
import { LoginData, LoginError, UserError } from '../Models/user.model';
import { loginReq } from '../store/user.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent {
  @ViewChild('f') form!: NgForm;
  error: Observable<null | LoginError>;
  loading: Observable<boolean>;
  constructor(private store: Store<AppState>) {
    this.error = store.select(state => state.user.loginError);
    this.loading = store.select(state => state.user.loginLoad);
  }



  onLogin() {
    const userData: LoginData = this.form.value;
    this.store.dispatch(loginReq({userData}));
  }
}
