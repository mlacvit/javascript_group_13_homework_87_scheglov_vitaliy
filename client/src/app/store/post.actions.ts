import { createAction, props } from '@ngrx/store';
import { PostData, PostModel } from '../Models/post.model';

export const fetchPostReq = createAction('[Post] post Req');
export const fetchPostSusses = createAction(
  '[Post] Post susses',
  props<{post: PostModel[]}>()
);
export const fetchPostFail = createAction(
  '[Post] Post Fail',
  props<{error: string}>()
);

export const createPostReq = createAction(
  '[Post] create Req',
  props<{postData: PostData}>()
);
export const createPostSusses = createAction(
  '[Post] create susses');
export const createPostFail = createAction(
  '[Post] Post Fail',
  props<{error: string}>()
);
