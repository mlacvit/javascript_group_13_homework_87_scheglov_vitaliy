import { PostState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createPostFail,
  createPostReq,
  createPostSusses,
  fetchPostFail,
  fetchPostReq,
  fetchPostSusses
} from './post.actions';

const initialState: PostState = {
  post: [],
  loading: false,
  error: null,
  createLoading: false,
  createError: null
};

export const postReducer = createReducer(
  initialState,
  on(fetchPostReq, state => ({...state, loading: true})),
  on(fetchPostSusses, (state, {post}) => ({
    ...state,
    loading: false,
    post

  })),
  on(fetchPostFail, (state, {error}) => ({
    ...state,
    loading: false,
    fError: error
  })),

  on(createPostReq, state => ({...state, loading: true})),
  on(createPostSusses, state => ({
    ...state,
    loading: false,
  })),
  on(createPostFail, (state, {error}) => ({
    ...state,
    loading: false,
    fError: error
  }))
);
