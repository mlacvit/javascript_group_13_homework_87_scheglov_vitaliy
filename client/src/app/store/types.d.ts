import { PostModel } from '../Models/post.model';
import { LoginError, User, UserError } from '../Models/user.model';


export type PostState = {
  post: PostModel[],
  loading: boolean,
  error: null | string,
  createLoading: boolean,
  createError: null | string,
};

export type AppState = {
  post: PostState,
  user: UserState
}

export type UserState = {
  user: null | User,
  regload: boolean,
  regError: null | UserError,
  loginLoad: boolean,
  loginError: null | LoginError
}
