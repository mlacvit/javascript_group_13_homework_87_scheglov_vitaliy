import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import {
  createPostFail,
  createPostReq,
  createPostSusses,
  fetchPostFail,
  fetchPostReq,
  fetchPostSusses
} from './post.actions';
import { catchError, mergeMap, of, tap } from 'rxjs';
import { map } from 'rxjs/operators';
import { PostService } from '../services/post.services';

@Injectable()
export class PostEffects {
  fetchPost = createEffect(() => this.actions.pipe(
    ofType(fetchPostReq),
    mergeMap(() => this.service.getPost().pipe(
      map(post => fetchPostSusses({post})),
        tap(() => this.router.navigate(['/'])),
      catchError(() => of(fetchPostFail({
        error: 'Something went wrong'
      })))
    ))
  ));

  createPostEffect = createEffect(() => this.actions.pipe(
    ofType(createPostReq),
    mergeMap(({postData}) => this.service.createPost(postData).pipe(
      map(() => createPostSusses()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(createPostFail({error: 'wrong data'})))
    ))
  ));

  constructor(
    private actions: Actions,
    private service: PostService,
    private router: Router
  ) {}
}
