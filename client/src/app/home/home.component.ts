import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../store/types';
import { Observable } from 'rxjs';
import { PostModel } from '../Models/post.model';
import { fetchPostReq } from '../store/post.actions';
import { User } from '../Models/user.model';
import { logOut } from '../store/user.actions';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  post: Observable<PostModel[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;


  constructor(private store: Store<AppState>) {
    this.post = store.select(state => state.post.post);
    this.loading = store.select(state => state.post.loading);
    this.error = store.select(state => state.post.error);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchPostReq());
    this.post.forEach(user => {
    })
  }

}
