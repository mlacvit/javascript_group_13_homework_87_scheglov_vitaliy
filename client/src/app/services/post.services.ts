import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PostData, PostModel } from '../Models/post.model';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  constructor(private http: HttpClient) {}

  getPost() {
    return this.http.get<PostModel[]>('http://localhost:8000/post');
  }
  createPost(postData: PostData){
    return this.http.post('http://localhost:8000/post', postData);
  }
}
