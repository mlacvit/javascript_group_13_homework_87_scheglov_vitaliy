import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../store/types';
import { UserError } from '../Models/user.model';
import { regUserReq } from '../store/user.actions';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.sass']
})
export class UserComponent implements AfterViewInit, OnDestroy {
  @ViewChild('f') form!: NgForm;
  error: Observable<null | UserError>;
  loading: Observable<boolean>;
  errorSub!: Subscription;
  constructor(private store: Store<AppState>) {
    this.error = store.select(state => state.user.regError);
    this.loading = store.select(state => state.user.regload);
  }


  onSubRegister() {
    this.store.dispatch(regUserReq({userData: this.form.value}))
  }
  ngAfterViewInit(): void {
    this.errorSub = this.error.subscribe(error => {
      if (error){
        const msg = error.errors.email.message;
        this.form.form.get('email')?.setErrors({serverError: msg})
      }else {
        this.form.form.get('email')?.setErrors({})
      }
    })
  }

  ngOnDestroy(): void {
    this.errorSub.unsubscribe();
  }
}
