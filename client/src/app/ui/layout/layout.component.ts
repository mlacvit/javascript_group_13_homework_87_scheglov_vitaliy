import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { User } from '../../Models/user.model';
import { logOut, logOutReq } from '../../store/user.actions';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit{
  user: Observable<null | User>;
  name!: User | null;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private store: Store<AppState>
  ) {
    this.user = store.select(state => state.user.user);
  }

  logOut(){
    this.store.dispatch(logOutReq());
    this.store.dispatch(logOut());
  }

  ngOnInit(): void {
    this.user.forEach(user => {
      return this.name = user;
    })
  }

}
