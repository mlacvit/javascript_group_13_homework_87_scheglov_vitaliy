import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../store/types';
import { PostData } from '../Models/post.model';
import { createPostReq } from '../store/post.actions';
import { User } from '../Models/user.model';
import { regUserReq } from '../store/user.actions';

@Component({
  selector: 'app-usnew',
  templateUrl: './usnew.component.html',
  styleUrls: ['./usnew.component.sass']
})
export class UsnewComponent {
  @ViewChild('f') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  user: Observable<null | User>
  constructor(private store: Store<AppState>) {
    this.loading = store.select(state => state.post.createLoading);
    this.error = store.select(state => state.post.createError);
    this.user = store.select(state => state.user.user)
  }

  onSub() {
    const postData: PostData = this.form.value;
    this.store.dispatch(createPostReq({postData}));
  }

}
