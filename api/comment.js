const express = require('express');
const Comment = require("./models/Comment");
const router = express.Router();


router.get('/', async (req, res, next) => {
  try {
    const comment = await Comment.find();
    return res.send(comment);
  } catch (e) {
    next(e);
  }
});

router.post('/', async (req, res, next) => {
  try {
    if (!req.body.title) {
      return res.status(400).send({message: 'title are required'});
    }

    const commentData = {
      title: req.body.title,
      description: req.body.description,
      image: null,
    };

    if (req.file) {
      commentData.image = req.file.filename;
    }

    const commentBase = new Comment(commentData);

    await commentBase.save();

    return res.send({message: 'Created new artist', id: commentBase._id});
  } catch (e) {
    next(e);
  }
});

module.exports = router;