const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ComSchema = new Schema({

  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
  },
  post: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'post',
  },
  description: String,

});

const Comment = mongoose.model('Comment', ComSchema);

module.exports = Comment;