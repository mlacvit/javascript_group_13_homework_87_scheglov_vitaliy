const express = require('express');

const Post = require("../models/Post");
const User = require("../models/User");
const auth = require("../middleware/auth");
const mongoose = require("mongoose");

const router = express.Router();


router.get('/', async (req, res, next) => {
  try {
    const post = await Comment.find().populate('user');
    return res.send(post);
  } catch (e) {
    next(e);
  }
});

router.post('/', auth, async (req, res, next) => {
  try {

    const comData = req.body;
    const token = req.get('Authorization');
    comData.user = await User.findOne({token});
    comData.post = await Post.findById(req.body.post);
    const comBase = new Comment(comData);

    await comBase.save();

    return res.send({message: 'Created new comment', id: comBase._id});
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError){
      return res.status(400).send(e);
    }
    next(e);
  }
});

module.exports = router;