const express = require('express');
const config = require('../config');
const path = require('path');
const Post = require("../models/Post");
const User = require("../models/User");
const { nanoid } = require('nanoid');
const multer = require('multer');


const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    const post = await Post.find().populate('user');
    return res.send(post);
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const post = await Post.findById(req.params.id);
    if (!post) {
      return res.status(404).send({message: 'Not found'});
    }
    return res.send(post);
  } catch (e) {
    next(e);
  }
});

router.post('/', upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.title) {
      return res.status(400).send({message: 'title are required'});
    }
    const postData = {
      title: req.body.title,
      description: req.body.description,
      image: null,
      date: new Date(),
    };

    const token = req.get('Authorization');
    postData.user = User.findOne({token});
    if (req.file) {
      postData.image = req.file.filename;
    }

    const postBase = new Post(postData);

    await postBase.save();

    return res.send({message: 'Created new post', id: postBase._id});
  } catch (e) {
    next(e);
  }
});

module.exports = router;